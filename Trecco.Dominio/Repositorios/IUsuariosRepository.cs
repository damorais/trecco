﻿using Trecco.Dominio.Entidades;

namespace Trecco.Dominio.Repositorios
{
    public interface IUsuariosRepository
    {
        Usuario CriarUsuario(Usuario usuario);
        Usuario ObterUsuario(string email);
    }
}