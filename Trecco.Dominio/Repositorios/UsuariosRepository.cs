﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trecco.Dominio.Entidades;

namespace Trecco.Dominio.Repositorios
{
    public class UsuariosRepository : IUsuariosRepository
    {
        private static Dictionary<string, Usuario> DictionaryUsuarios = new Dictionary<string, Usuario>();

        public Usuario ObterUsuario(string email)
        {
            return DictionaryUsuarios.ContainsKey(email) ? DictionaryUsuarios[email] : null;
        }

        public Usuario CriarUsuario(Usuario usuario)
        {
            if (!DictionaryUsuarios.ContainsKey(usuario.Email))
                DictionaryUsuarios.Add(usuario.Email, usuario);
            else
                throw new ArgumentException($"Já existe um usuario com o e-mail {usuario.Email}");

            return usuario;
        }
    }
}
