﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trecco.Dominio.Entidades;

namespace Trecco.Dominio.Repositorios
{
    public interface IQuadrosRepository
    {
        QuadroID ObterProximoID();
        Quadro Obter(QuadroID id);

        IEnumerable<Quadro> ObterTodos();
        IEnumerable<Quadro> ObterAtivos();
        IEnumerable<Quadro> ObterInativos();

        bool ExcluirPermanentemente(QuadroID id);
        void Salvar(Quadro quadro);
        void Restaurar(QuadroID id);
        void Inativar(QuadroID id);
    }
}
