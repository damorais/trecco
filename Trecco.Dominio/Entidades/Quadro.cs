﻿using System;
using System.Collections.Generic;

namespace Trecco.Dominio.Entidades
{
    /// <summary>
    /// Esta estrutura é para representar um ID de quadro. A ideia aqui é prevenir que eu utilize um método
    /// que deveria utilizar um ID de quadro, passando um outro valor qualquer.
    /// Um Struct, no caso é um tipo de "classe leve". Representa um tipo de dado composto, porém, diferente de
    /// uma classe, que trabalha utilizando referências, um Struct usa passagem por valor, ou seja, se comporta
    /// de forma mais próxima a um int do que a um objeto.
    /// </summary>
    public struct QuadroID
    {
        //Se eu não defino o set em uma propriedade, seu valor só pode ser atribuído no construtor.
        public int Valor { get; }
        public QuadroID(int valor)
        {
            this.Valor = valor;
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj.GetType() == this.GetType())
                return ((QuadroID)obj).Valor == this.Valor;
            else
                return false;
        }

        public override int GetHashCode()
        {
            return 991088425 + Valor.GetHashCode();
        }

        public override string ToString()
        {
            return $"QuadroID({this.Valor})";
        }
    }

    public class Quadro
    {
        public QuadroID Id { get; private set; }
        public string Nome { get; private set; }
        public bool Ativo { get; private set; }
        public DateTime DataDeCriacao { get; private set; }       
        public DateTime DataDeAlteracao { get; private set; }

        public Quadro(QuadroID id, string nome)
        {
            this.Id = id;
            this.Nome = nome;
            this.DataDeCriacao = DateTime.Now;
            this.DataDeAlteracao = DateTime.Now;
            this.Ativo = true;
        }

        public void Restaurar()
        {
            //Altero o nome do quadro, para evitar que exista dois quadros com o mesmo nome. Para isso, 
            //adiciono a data e hora da restauração
            this.Nome = $"{this.Nome} (Restaurado em {DateTime.Now.ToString("dd-mm-yyyy HH:mm:ss")})";
            this.Ativo = true;
            this.DataDeAlteracao = DateTime.Now;
        }

        public void Inativar()
        {
            this.Ativo = false;
            this.DataDeAlteracao = DateTime.Now;
        }

        //TODO: Aqui vai entrar o mecanismo de validação... Novo nome já existe???
        public void AlterarNome(string NovoNome)
        {
            this.Nome = NovoNome;
            this.DataDeAlteracao = DateTime.Now;
        }

        public override bool Equals(object obj)
        {
            var that = obj as Quadro;
            return that != null && this.Id.Equals(that.Id) && this.Nome == that.Nome;
        }

        public override int GetHashCode()
        {
            var hashCode = -1643562096;
            hashCode = hashCode * -1521134295 + Id.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Nome);
            return hashCode;
        }
    }
}