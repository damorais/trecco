﻿using System.Collections.Generic;
using Trecco.Dominio.Entidades;

namespace Trecco.Dominio.Servicos
{
    public interface IQuadrosService
    {
        void AtualizarQuadro(QuadroID quadroID, string novoNome);
        void CriarQuadro(string nome);
        bool ExcluirPermanentemente(QuadroID quadroID);
        void InativarQuadro(QuadroID quadroID);
        Quadro ObterQuadro(QuadroID quadroID);
        IEnumerable<Quadro> ObterQuadros();
        IEnumerable<Quadro> ObterQuadrosInativos();
        void ReativarQuadro(QuadroID quadroID);
    }
}