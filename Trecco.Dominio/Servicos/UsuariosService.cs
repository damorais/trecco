﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trecco.Dominio.Entidades;
using Trecco.Dominio.Repositorios;

namespace Trecco.Dominio.Servicos
{
    internal class UsuariosService : IUsuariosService
    {
        private UsuariosRepository _repository;
        public UsuariosService()
        {
            this._repository = new UsuariosRepository();
        }

        public bool ExisteUsuario(string email) => _repository.ObterUsuario(email) != null;

        public Usuario ObterUsuario(string email) => _repository.ObterUsuario(email);

        public Usuario CriarUsuario(string email, string nome, string sobrenome, string senha)
        {
            if(_repository.ObterUsuario(email) == null)
            {
                var novoUsuario = Usuario.Create(email, nome, sobrenome, senha);

                return _repository.CriarUsuario(novoUsuario);
            }
            else
            {
                throw new ArgumentException("Já existe um usuário com este e-mail");
            }
        }
    }
}
