﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Trecco.ViewModels.Usuarios
{
    public class CadastroForm
    {
        [DisplayName("Primeiro Nome")]
        [Required(ErrorMessage = "Este campo é obrigatório.")]
        [MaxLength(length: 50, ErrorMessage ="Este campo deve possuir, no máximo, 100 caracteres.")]
        public string Nome { get; set; }

        [DisplayName("Sobrenome")]
        [Required(ErrorMessage = "Este campo é obrigatório.")]
        [MaxLength(length: 200, ErrorMessage = "Este campo deve possuir, no máximo, 200 caracteres.")]
        public string Sobrenome { get; set; }

        [DisplayName("E-mail")]
        [Required(ErrorMessage = "Este campo é obrigatório.")]
        [EmailAddress(ErrorMessage = "Deve ser informado um e-mail válido.")]
        [MaxLength(length: 50, ErrorMessage = "Este campo deve possuir, no máximo, 50 caracteres.")]
        public string Email { get; set; }

        [DisplayName("Senha")]
        [Required(ErrorMessage = "Este campo é obrigatório.")]
        [MinLength(length:8, ErrorMessage = "A senha deve conter ao menos 8 caracteres.")]
        public string Senha { get; set; }

        [DisplayName("Confirmar Senha")]
        [Required(ErrorMessage = "Este campo é obrigatório.")]
        [Compare(otherProperty: "Senha", ErrorMessage = "A confirmação de senha deve ser igual a senha.")]
        public string ConfirmarSenha { get; set; }
    }
}