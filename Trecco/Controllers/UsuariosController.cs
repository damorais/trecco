﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Trecco.Dominio.Fabricas;
using Trecco.Dominio.Servicos;
using Trecco.ViewModels.Usuarios;

namespace Trecco.Controllers
{
    public class UsuariosController : Controller
    {
        private IUsuariosService _service;

        public UsuariosController()
        {
            _service = UsuariosServiceFactory.CriarUsuariosService();
        }


        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CadastroForm formulario)
        {
            if (!ModelState.IsValid) return View(formulario);

            if (_service.ExisteUsuario(formulario.Email))
            {
                ModelState.AddModelError("Email", "Já existe um usuário com este e-mail");
                return View(formulario);
            }

            _service.CriarUsuario(formulario.Email, formulario.Nome, formulario.Sobrenome, formulario.Senha);

            return RedirectToAction("List", "Quadros");
        }
    }
}