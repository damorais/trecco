﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Trecco.Dominio.Entidades;
using Trecco.Dominio.Servicos;

namespace Trecco.Controllers
{
    public class LixeiraController : Controller
    {
        private readonly IQuadrosService _service;

        public LixeiraController()
        {
            _service = new QuadrosService();
        }

        public ActionResult List()
        {
            var quadros = _service.ObterQuadrosInativos();

            return View(quadros);
        }

        public ActionResult Delete(int id)
        {
            _service.ExcluirPermanentemente(new QuadroID(id));

            TempData["Mensagem"] = "Quadro removido.";

            return RedirectToAction("list");
        }

        public ActionResult Restore(int id)
        {
            _service.ReativarQuadro(new QuadroID(id));

            TempData["Mensagem"] = "Quadro restaurado.";

            return RedirectToAction("list", "quadros");
        }
    }
}